function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function setCookie(name, value, options) {
    options = options || {};
    var expires = options.expires;
    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function getCookie(name) {
    var mas = document.cookie.match(name + "=[^;]*");
    if (mas) {
        var cook = mas[0].split(/=/);
        return decodeURIComponent(cook[1])
    }
    ;
    return undefined;
}

function checkCollision(ball, ball2) {
    var dx = ball.x - ball2.x;
    var dy = ball.y - ball2.y;
    var distance = Math.sqrt(dx * dx + dy * dy);
    var minDistance = ball.r + ball2.r;
    var ang = Math.atan2(dy, dx);
    var d1 = Math.atan2(ball.vy, ball.vx);
    var d2 = Math.atan2(ball2.vy, ball2.vx);
    var v1 = Math.sqrt(ball.vx * ball.vx + ball.vy * ball.vy);
    var v2 = Math.sqrt(ball2.vx * ball2.vx + ball2.vy * ball2.vy);
    var newVx = v1 * Math.cos(d1 - ang);
    var newVy = v1 * Math.sin(d1 - ang);
    var newVx2 = v2 * Math.cos(d2 - ang);
    var newVy2 = v2 * Math.sin(d2 - ang);
    if (distance < minDistance) {
        ball2.vx = newVx2;
        ball2.vy = newVy2;
        ball.vx = newVx;
        ball.vy = newVy;
    }
}
var selected = false;

function init() {
    var canvas = document.getElementById("canvas");
    if (canvas && canvas.getContext) {
        var context = canvas.getContext("2d");
    }
    var mouse = {
        x: 0,
        y: 0,
        down: false
    }
    var leftContainer = {
        x: 0,
        y: 0,
        w: 500,
        h: 500
    };
    var rightContainer = {
        x: 530,
        y: 0,
        w: 500,
        h: 500
    };
    var balls = [];
    for (var i = 0; i < 5; i++) {
        if (getCookie('ballsState' + i)) {
            var ballsString = getCookie('ballsState' + i);
            balls[i] = JSON.parse(ballsString);
        } else {
            balls[i] = {
                x: getRandomInt(10, 40) * 10 + 20,
                y: getRandomInt(20, 300),
                r: 10,
                vx: getRandomInt(5, 10),
                vy: getRandomInt(-5, 5),
                color: getRandomInt(100, 200)
            }
        }
    }
    document.addEventListener('touchmove', function (e) {
        e.preventDefault();
        e.stopPropagation();
        mouse.x = e.touches[0].pageX - canvas.getBoundingClientRect().left
        mouse.y = e.touches[0].pageY - canvas.getBoundingClientRect().top
    }, false);
    document.addEventListener('touchstart', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (!selected) {
            var x = e.touches[0].pageX - canvas.getBoundingClientRect().left;
            var y = e.touches[0].pageY - canvas.getBoundingClientRect().top;
            for (var i = 0; i < 5; i++) {
                var dx = x - balls[i].x,
                    dy = y - balls[i].y,
                    dist = Math.sqrt(dx * dx + dy * dy);
                if (dist < balls[i].r) {
                    e.target.style.cursor = 'pointer';
                    selected = balls[i];
                }
            }
        }
    }, false);
    document.onmousemove = function (e) {
        mouse.x = e.pageX - canvas.getBoundingClientRect().left
        mouse.y = e.pageY - canvas.getBoundingClientRect().top
    };
    document.onmousedown = function (e) {
        if (!selected) {
            var x = e.pageX - canvas.getBoundingClientRect().left;
            var y = e.pageY - canvas.getBoundingClientRect().top;
            for (var i = 0; i < 5; i++) {
                var dx = x - balls[i].x,
                    dy = y - balls[i].y,
                    dist = Math.sqrt(dx * dx + dy * dy);
                if (dist < balls[i].r) {
                    e.target.style.cursor = 'pointer';
                    selected = balls[i];
                }
            }
        }
    }
    document.addEventListener('touchend', function (e) {
        e.preventDefault();
        e.stopPropagation();
        mouse.down = false;
        selected = false;
    }, false);
    document.onmouseup = function (e) {
        mouse.down = false;
        selected = false;
    }

    function draw() {
        context.strokeStyle = "lightgray";
        context.strokeRect(leftContainer.x, leftContainer.y, leftContainer.w, leftContainer.h);
        context.strokeRect(rightContainer.x, rightContainer.y, rightContainer.w, rightContainer.h);
        context.clearRect(rightContainer.x, rightContainer.y, rightContainer.w, rightContainer.h);
        context.clearRect(leftContainer.x, leftContainer.y, leftContainer.w, leftContainer.h);
        context.clearRect(501, 0, 28, 500);
        for (var i = 0; i < balls.length; i++) {
            context.fillStyle = 'hsl(' + balls[i].color++ + ', 100%, 50%)';
            context.beginPath();
            context.arc(balls[i].x, balls[i].y, balls[i].r, 0, Math.PI * 2, true);
            context.closePath();
            context.fill();
            if (selected) {
                selected.x = mouse.x;
                selected.y = mouse.y;
            }
            if (balls[i].x > 540) {
                if ((balls[i].x + balls[i].vx + balls[i].r >= rightContainer.x + rightContainer.w) || (balls[i].x - balls[i].r + balls[i].vx <= rightContainer.x)) {
                    balls[i].vx = -balls[i].vx;
                }
                if ((balls[i].y + balls[i].vy + balls[i].r >= rightContainer.y + rightContainer.h) || (balls[i].y - balls[i].r + balls[i].vy <= rightContainer.y)) {
                    balls[i].vy = -balls[i].vy;
                }
                balls[i].x += balls[i].vx;
                balls[i].y += balls[i].vy;
                for (var j = balls.length - 1; j >= 0; j--)
                    if (i != j) {
                        checkCollision(balls[i], balls[j]);
                    }
            }
            var ballsStateStrings = JSON.stringify(balls[i]);
            setCookie(("ballsState" + i), ballsStateStrings);
        }
        requestAnimationFrame(draw);
    };
    requestAnimationFrame(draw);
}
window.onload = function () {
    init();
};